/* 
 * File:   main.c
 * Author: savinov
 *
 * Created on February 18, 2015, 7:14 PM
 */

#include <stdio.h>
#include <stdlib.h>
#include <errno.h>

static const int NOT_FOUND_INDEX = -1;

int bsearch_impl(int* array, int size, int x, int start_index, int end_index) {
    if(end_index >= size || start_index < 0 || start_index > end_index) {
        return NOT_FOUND_INDEX;
    }
    int med = start_index + (end_index - start_index)/2;
    int index = NOT_FOUND_INDEX;
    if(array[med] <= x ) {
        int res = bsearch_impl(array, size, x, med +1, end_index);
        if(res != NOT_FOUND_INDEX ) {
            index = res;
        }
    } else {
        index = med;
        int res = bsearch_impl(array, size, x, start_index, med -1);
        if(res != NOT_FOUND_INDEX && res < index) {
            index = res;
        }
    }
    return index;
}

int searchb(int* array, int size, int x) {
    if(size <= 0 || array == NULL) {
        return NOT_FOUND_INDEX;
    }
    return bsearch_impl(array, size, x, 0, size -1);
}

void test_search_with_print(int* array, int size, int x) {
    printf("search number more than %d in array :{", x);
    for(int i = 0; i < size; i++) {
        printf("%d,", array[i]);
    }
    printf("}\n");
    int index = searchb(array, size, x);
    if(index == NOT_FOUND_INDEX) {
        printf("answere:not found");
    } else {
        printf("answere:index=%d", index);
    }
    printf("\n\n");
}

void test_search() {
    const int search_element = 5;
    int array1[] = {};
    int array2[] = {4};
    int array3[] = {5};
    int array4[] = {6};
    int array5[] = {3, 5, 6, 7};
    int array6[] = {1, 5, 6, 6, 6, 6, 6};
    int array7[] = {1, 5, 6, 6, 6, 6, 6, 7, 8};
    int array8[] = {1, 2, 3, 4, 5};
    int array9[] = {1, 5, 7, 8};
    int array10[] = {1, 5, 7, 7, 8};
    
    test_search_with_print(array1, sizeof(array1)/sizeof(int), search_element);
    test_search_with_print(array2, sizeof(array2)/sizeof(int), search_element);
    test_search_with_print(array3, sizeof(array3)/sizeof(int), search_element);
    test_search_with_print(array4, sizeof(array4)/sizeof(int), search_element);
    test_search_with_print(array5, sizeof(array5)/sizeof(int), search_element);
    test_search_with_print(array6, sizeof(array6)/sizeof(int), search_element);
    test_search_with_print(array7, sizeof(array7)/sizeof(int), search_element);
    test_search_with_print(array8, sizeof(array8)/sizeof(int), search_element);
    test_search_with_print(array9, sizeof(array9)/sizeof(int), search_element);
    test_search_with_print(array10, sizeof(array10)/sizeof(int), search_element);
}


int main(int argc, char** argv) {
    test_search();
    return (EXIT_SUCCESS);
}

