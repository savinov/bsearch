BIN_NAME=bsearch

all: $(BIN_NAME)

$(BIN_NAME): main.o 
	gcc main.o -o $(BIN_NAME)

main.o: main.c
	gcc -std=c99 -c main.c

clean:
	rm -rf *.o $(BIN_NAME)
